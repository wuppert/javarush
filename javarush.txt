package com.javarush.test.level04.lesson02.task01;

/* ����������� ����� setName
����������� ����� setName, ����� � ��� ������� ����� ���� ������������� �������� ���������� private String name 
������ ����������� ��������� String name.
*/

public class Cat {
    private String name;

    public void setName(String name) {
        this.name = name;//�������� ��� ��� ���
    }
}




package com.javarush.test.level04.lesson02.task02;

/* ����������� ����� addNewCat
����������� ����� addNewCat, ����� ��� ��� ������ (�.�. ���������� ������ ����), ���������� ����� ������������� 
�� 1. �� ���������� ����� �������� ���������� catsCount.
*/

public class Cat {
    private static int catsCount = 0;

    public static void addNewCat() {
        catsCount = catsCount +1 ; //�������� ��� ��� ���
    }
}


package com.javarush.test.level04.lesson02.task03;

/* ����������� ����� setCatsCount
����������� ����� setCatsCount ���, ����� � ��� ������� ����� ���� ������������� �������� ���������� catsCount 
������ ����������� ���������.
*/

public class Cat {
    private static int catsCount = 0;

    public static void setCatsCount(int catsCount) {
       Cat.catsCount = catsCount; //�������� ��� ��� ���
    }
}





package com.javarush.test.level04.lesson02.task04;

/* ����������� ����� setName
����������� ����� setName, ����� � ��� ������� ����� ���� ������������� �������� ���������� 
private String fullName ������ �������� ��������� ���������� String fullName.
*/

public class Cat {
    private String fullName;

    public void setName(String firstName, String lastName) {
        String fullName = firstName + " " + lastName;

       this.fullName = fullName; //�������� ��� ��� ���
    }
}




���������
package com.javarush.test.level04.lesson02.task05;

/* ���������� ���������� �����
�������� ���, ����� ��������� ��������� ���������� ��������� ����� (count) � �� ����� ���������� 
��������� �� ����������.
*/

public class Solution
{
    public static void main(String[] args)
    {
        Cat cat1 = new Cat();
        Cat.count = Cat.count+1;//�������� ��� ��� ���

        Cat cat2 = new Cat();
        Cat.count = Cat.count+1;//�������� ��� ��� ���

        System.out.println("Cats count is " + Cat.count);
    }
    public static class Cat
    {
        public static int count = 0;
    }
}




package com.javarush.test.level04.lesson04.task04;

/* ����� ����
����������� ����� checkSeason. �� ������ ������, ����� ������ ���������� ����� ���� (����, �����, ����, �����) � ������� �� �����.
������ ��� ������ ������ 2:
����
������ ��� ������ ������ 5:
�����
*/

public class Solution
{
    public static void main(String[] args) {
        checkSeason(12);
        checkSeason(4);
        checkSeason(7);
        checkSeason(10);
    }

    public static void checkSeason(int mount){
        if (mount >=1 && mount <=2 || mount == 12)
            System.out.println("����");
        else if (mount >= 3 && mount <=5)
            System.out.println("�����");
        else if (mount >= 6 && mount <= 8)
            System.out.println("����");
        else if (mount >= 9 && mount <= 11)
            System.out.println("�����");
    }
}


/* ������������� � ������������� �����
������ � ���������� �����. ���� ����� �������������, �� ��������� ��� � ��� ����. ���� ����� �������������, �� ��������� �������.
������� ��������� �� �����.
*/

import java.io.*;

        public class Solution
        {
            public static void main(String[] args) throws Exception
            {
                BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));
                String inputNumber = bufReader.readLine();
                Integer input2 = Integer.valueOf(inputNumber);
                if (input2 > 0){ input2 = input2 * 2; System.out.println(input2); }
                else{ input2++; System.out.println(input2); }

            }

}


������ � ���������� ����� ��� ������, � ����������� �� ������ ������� �������� ������������, ��������, ������, ��������, ��������, ��������, ������������,
���� ������ ����� ������ ��� ������ 7 � ������� ������� ��� ������ �� ����������.
������ ��� ������ 5:
�������
������ ��� ������ 10:
������ ��� ������ �� ����������
*/


 BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));
        String inputNumber = bufReader.readLine();
        Integer num = Integer.valueOf(inputNumber);
        if (num==1)
            System.out.println("�����������");
        else  if (num==2)
                System.out.println("�������");
              else if (num==3)
                    System.out.println("�����");
                    else if (num==4)
                         System.out.println("�������");
                          else if (num==5)
                                System.out.println("�������");
                                else if (num==6)
                                        System.out.println("�������");
                                        else if (num==6)
                                                System.out.println("�������");
                                              else if (num==7)
                                                    System.out.println("�����������");
                                                    else if (num>7 || num<0 )
                                                            System.out.println("������ ��� ������ �� ����������");









/* ��� �����
������ � ���������� ��� ����� �����. ����������, ������� �� ����� ��� ���� �� ���� ���� ������ ����� ����� �����.
���� ����� ���� ����������, ������� �� ����� ����� ����� ������. ���� ��� ��� ����� ����� ����� �����, �� ������� ��� ���.
������ ��� ����� 1 2 2:
2 2
������ ��� ����� 2 2 2:
2 2 2
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));
        String inputNumber1 = bufReader.readLine();
        String inputNumber2 = bufReader.readLine();
        String inputNumber3 = bufReader.readLine();
        Integer a = Integer.valueOf(inputNumber1);
        Integer b = Integer.valueOf(inputNumber2);
        Integer c = Integer.valueOf(inputNumber3);
        if(a == b & a != c & b != c)
            System.out.println(a + " " + b);
        else
            System.out.println();
        if(a == c & a != b & b != c)
            System.out.println(a + " " + c);
        else
            System.out.println();
        if(c == b & a != c & a != b)
            System.out.println(c + " " + b);
        else
            System.out.println();
        if(c == b & a==c & b==a)
            System.out.println(c + " " + b + " " + a);
        else
            System.out.println();




/* �����������
������ � ���������� ��� ����� �, b, c � ������� ��������������� ������������.
���������� ����������� ������������� ������������ �� ��������. ��������� ������� �� ����� � ��������� ����:
"����������� ����������." - ���� ����������� � ������ ��������� ����������.
"����������� �� ����������." - ���� ����������� � ������ ��������� �� ����������.
���������: ����������� ���������� ������ �����, ����� ����� ����� ���� ��� ������ ������ �������.
��������� �������� ������ ������� � ������ ���� ������.
���� ���� �� � ����� ������ ������� �������� ������ ����� ���� ������, �� ������������ � ������ ��������� �� ����������.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));
        String inputNumber1 = bufReader.readLine();
        String inputNumber2 = bufReader.readLine();
        String inputNumber3 = bufReader.readLine();
        Integer a = Integer.valueOf(inputNumber1);
        Integer b = Integer.valueOf(inputNumber2);
        Integer c = Integer.valueOf(inputNumber3);
        if (a < b+c && b < a+c && c < a+b)
        {
            System.out.println("����������� ����������.");
        }
        else
        {
            System.out.println("����������� �� ����������.");
        }
    }
}



/* ������� ���� �����
������ � ���������� ��� �����, � ������� �� ����� ����������� �� ���.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));
        String inputNumber1 = bufReader.readLine();
        String inputNumber2 = bufReader.readLine();
        Integer a = Integer.valueOf(inputNumber1);
        Integer b = Integer.valueOf(inputNumber2);
        if (a<b)
            System.out.println(a);
         else
            System.out.println(b);
}




/* �������� ������� �����
������ � ���������� ������ �����, � ������� ������������ �� ���.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));
        String inputNumber1 = bufReader.readLine();
        String inputNumber2 = bufReader.readLine();
        String inputNumber3 = bufReader.readLine();
        String inputNumber4 = bufReader.readLine();
        Integer a = Integer.valueOf(inputNumber1);
        Integer b = Integer.valueOf(inputNumber2);
        Integer c = Integer.valueOf(inputNumber3);
        Integer d = Integer.valueOf(inputNumber4);

        if ((a>b) && (a>c) && (a>d))
            System.out.println(a);
        else
            if ((b>a) && (b>c) && (b>d))
                System.out.println(b);
            else if ((c>a) && (c>b) && (c>d))
                System.out.println(c);
                 else if ((d>a) && (d>b) && (d>c))
                System.out.println(d);
    }
}




import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String name = reader.readLine();
        String sAge = reader.readLine();
        int age = Integer.parseInt(sAge);
        if (age < 18)
            System.out.println("�������� ���");


    }
}




/* � 18-�� ����������
������ � ���������� ��� � �������. ���� ������� ������ 20 ������� ������� �� 18-�� ����������.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String name = reader.readLine();
        String sAge = reader.readLine();
        int age = Integer.parseInt(sAge);
        if (age>20)
            System.out.println("� 18-�� ����������");

    }
}


/* ������ - ��������
������ � ���������� ����� �����. ������� �� ����� ��� ������-�������� ���������� ����:
�������������� ������ ����� - ���� ����� ������������� � ������,
�������������� �������� ����� - ���� ����� ������������� � ��������,
�������� ����� - ���� ����� ����� 0,
�������������� ������ ����� - ���� ����� ������������� � ������,
�������������� �������� ����� - ���� ����� ������������� � ��������.
������ ��� ����� 100:
������������� ������ �����
������ ��� ����� -51:
������������� �������� �����
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));
        String inputNumber1 = bufReader.readLine();
        Integer a = Integer.valueOf(inputNumber1);
        if (a==0)
            System.out.println("������� �����");
        else if (a>0){
                    if (a % 2 == 0)
                    System.out.println("������������� ������ �����");
                    else
                    System.out.println("������������� �������� �����");
                    }
                    if (a<0){
                        if (a % 2 == 0)
                            System.out.println("������������� ������ �����");
                        else
                            System.out.println("������������� �������� �����");
                    }



    }
}


public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));
        String inputNumber1 = bufReader.readLine();
        Integer a = Integer.valueOf(inputNumber1);
        if (a % 2 == 0)
        {
            if ((a >= 1) && (a < 10))
                System.out.print("������ ����������� �����");
            else if ((a > 9) && (a < 100))
                System.out.println("������ ���������� �����");
            else if ((a > 99) && (a < 1000))
                System.out.println("������ ����������� �����");

        }
        if (a % 2 == 1){
            if ((a >= 1) && (a < 10))
                System.out.print("�������� ����������� �����");
            else if ((a > 9) && (a < 100))
                   System.out.println("�������� ���������� �����");
                 else if ((a > 99) && (a < 1000))
                   System.out.println("�������� ����������� �����");


        }
    }
}



/* 10 �����
������� �� ����� ����� �� 1 �� 10 ��������� ���� while.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        int i = 1;
        while (i <= 10)
        {
            System.out.println(i);
            i++;

        }
    }
}

/* 10 ����� ��������
������� �� ����� ����� �� 10 �� 1 ��������� ���� while.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        int i = 10;
        while (i >= 1)
        {
            System.out.println(i);
            i--;

        }

    }
}

/* ������ �����
��������� ���� for ������� �� ����� ������ ����� �� 1 �� 100 ������������.
����� ������ ���� � ����� ������.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {

            for (int i = 2; i <= 100; i=i+2)
            {
                System.out.println(i+" ");
            }

        }
    }


/* � ������� �� ���� �������� �� �������
��������� ���� ������� �� ����� ��� ��� �������:
�� ������� �� ���� �������� �� �������. �����
*/

public class Solution
{
    public static void main(String[] args)
    {
        for (int i=1; i<=100;i++){
            System.out.println( "� ������� �� ���� �������� �� �������. �����");
        }
    }
}

/* ��� ����� �����
������ � ���������� ��� � ��������� ���� for 10 ��� �������: [*���* ����� ����.]
������ ������:
����� ����� ����.
����� ����� ����.
�
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String name = reader.readLine();


        for (int i=1;i<=10;i++)
            System.out.println(name+" ����� ����.");

    }
}
